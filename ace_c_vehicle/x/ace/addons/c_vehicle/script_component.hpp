#define COMPONENT c_vehicle
#include "\x\ace\addons\main\script_mod.hpp"

#ifdef DEBUG_ENABLED_C_VEHICLE
	#define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_C_VEHICLE
	#define DEBUG_SETTINGS DEBUG_SETTINGS_C_VEHICLE
#endif

#include "\x\ace\addons\main\script_macros.hpp"
